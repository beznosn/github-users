//
//  GFTabBarController.swift
//  GitHub Users
//
//  Created by Nick Beznos on 2/5/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit

class GFTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = .systemGreen
        viewControllers                 = [createSearchNC(), createFavotitesNC()]


        // Do any additional setup after loading the view.
    }
        
      
      func createSearchNC() -> UINavigationController {
          let searchVC = SearchVC()
          searchVC.title = "Search"
          searchVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
          
          return UINavigationController(rootViewController: searchVC)
      }
      
      
      func createFavotitesNC() -> UINavigationController {
          let favotitesVC = FavoritesListVC()
          favotitesVC.title = "Favorites"
          favotitesVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
          
          return UINavigationController(rootViewController: favotitesVC)
      }
}
