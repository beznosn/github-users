//
//  GFRepoItemVC.swift
//  GitHub Users
//
//  Created by Nick Beznos on 1/28/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit

class GFRepoItemVC: GFItemInfoVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        configureItems()
    }
    
    private func configureItems() {
        itemInfoViewOne.setItemInfoType(itemInfoType: .repos, with: user.publicRepos)
        itemInfoViewTwo.setItemInfoType(itemInfoType: .gists, with: user.publicGists)
        actionButton.set(backgroundColor: .systemPurple, title: "GitHub Profile")
    }
    
     
    override func actionButtonTapped() {
        delegate.didTapGitHubFollowersProfile(for: user)
    }
}

