//
//  Date+Ext.swift
//  GitHub Users
//
//  Created by Nick Beznos on 1/29/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

extension Date {
    func convertToMonthYearFormat() -> String {
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "MMM yyyy"
        return dateFormatter.string(from: self)
    }
    
    
}
