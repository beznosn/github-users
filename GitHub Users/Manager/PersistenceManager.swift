//
//  PersistenceManager.swift
//  GitHub Users
//
//  Created by Nick Beznos on 2/4/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

enum PersistenceActionType {
    case add, remove
}

enum PersistenceManager {
    static private let defaults = UserDefaults.standard
    
    
    enum Keys {
        static let favorites = "favorites"
    }
    
    
    static func updateWith(favorite: Follower, actionType: PersistenceActionType, completed: @escaping (GFError?) -> Void) {
        
        retriveFaforites { result in
            switch result {
            case .success(let favorites):
                var retrivedFaforites = favorites
                
                switch actionType {
                case .add:
                    guard !retrivedFaforites.contains(favorite) else {
                        completed(.alreadyInFavorites)
                        return
                    }
                    
                    retrivedFaforites.append(favorite)
                    
                case .remove:
                    retrivedFaforites.removeAll { $0.login == favorite.login }
                }
                completed(save(favorites: retrivedFaforites))

            case .failure(let error):
                completed(error)
            }
            
        }
        
    }
    
    
    static func retriveFaforites(completed: @escaping(Result<[Follower], GFError>) -> Void) {
        guard let favoritesData = defaults.object(forKey: Keys.favorites) as? Data else {
            completed(.success([]))
            return
        }
        
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let followers = try decoder.decode([Follower].self, from: favoritesData)
            completed(.success(followers))
        } catch {
            completed(.failure(.unableToFavorite))
        }
    }
    
    
    static func save(favorites: [Follower]) -> GFError? {
        do {
            let encoder  = JSONEncoder()
            let encodedFavorites = try encoder.encode(favorites)
            defaults.set(encodedFavorites, forKey: Keys.favorites)
            return nil
        } catch {
            return .unableToFavorite
        }
    }
    
}
