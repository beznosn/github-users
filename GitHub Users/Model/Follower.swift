//
//  Follower.swift
//  GitHub Users
//
//  Created by Nick Beznos on 1/23/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

struct Follower: Codable, Hashable {
    var login: String
    var avatarUrl: String
}
