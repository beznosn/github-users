//
//  FavoritesListVC.swift
//  GitHub Users
//
//  Created by Nick Beznos on 1/20/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit

class FavoritesListVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground

        PersistenceManager.retriveFaforites { result in
            switch result {
            case .success(let favorites):
                print(favorites)
            case .failure(let error):
                break
            }
            
        }
    }
    

}
