//
//  GFError.swift
//  GitHub Users
//
//  Created by Nick Beznos on 1/27/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

enum GFError: String, Error {
    case invalidUsername    = "This username created invalid request. Please try again."
    case invalidResponse    = "Invalid responce from server. Please try again."
    case invalidData        = "The data recived from the server was invalid. Please try again."
    case unableToComlete    = "Unable to complete your request. Please check your internet connection."
    case unableToFavorite   = "There was an error adding this user to favorites. Please try again later."
    case alreadyInFavorites = "This user is already in Favorites."
}
