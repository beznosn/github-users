//
//  UIHelper.swift
//  GitHub Users
//
//  Created by Nick Beznos on 1/25/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit


struct UIHelper {
    
    static func createThreeColumnColletionView(in view: UIView) -> UICollectionViewFlowLayout {
        let collectionWidth         = view.bounds.width
        let padding: CGFloat        = 12
        let minItemSpacing: CGFloat = 10
        let availableWidth          = collectionWidth - (padding * 2) - (minItemSpacing * 2)
        let itemWidth               = availableWidth/3
        
        let flowLayout              = UICollectionViewFlowLayout()
        flowLayout.sectionInset     = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        flowLayout.itemSize         = CGSize(width: itemWidth, height: itemWidth + 40)
        
        return flowLayout
    }
    
    
}
